# -*- coding: utf-8 -*-
from flask import render_template, render_template_string, request
from application import app
from .signature_form import SignatureForm
from .utils import render_jinja_html

@app.route('/', methods=['POST', 'GET'])
def index_sm():
    form = SignatureForm(request.form)
    # if request.method == 'POST' and form.validate():
    if request.method == 'POST':
        data = {
            'render_signature': True,
            'username': form.username.data,
            'useremail': form.useremail.data,
            'userphone': form.userphone.data,
            'RGPD' : form.RGPD.data,
            'publi' : form.publi.data,
            'form': False,
            'template_id': "_signature.html"
        }
        html_signature = render_jinja_html('signature.html', data)
        data['signature_raw'] = html_signature

    if request.method == 'GET':
        data = dict(render_signature=False, form=form)
    return render_template('index.html', data=data)

@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404
