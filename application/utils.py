import jinja2

def render_jinja_html(file_name,data):
    return jinja2.Environment(
        loader=jinja2.FileSystemLoader('application/templates/')
    ).get_template(file_name).render(data=data)